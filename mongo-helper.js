'use strict'

let mongodb = require('mongodb')
let MongoClient = mongodb.MongoClient
let assert = require('assert')
let config = require('./config')

let url = 'mongodb://' + config.db.user + ':' + config.db.password + '@' + config.db.host + ':' + config.db.port + '/' + config.db.db
let db = null
let collection = null

MongoClient.connect(url, function (err, dbConn) {
  assert.equal(null, err)
  console.log('Connected correctly to database')
  db = dbConn
  collection = db.collection('strategy')
})

let helper = {}

module.exports = helper

function query (id) {
  return {_id: new mongodb.ObjectID(id)}
}

helper.getStrategy = function (id) {
  return collection.findOne(query(id))
}

helper.updateStrategy = function (id, status, errorMessage, executable) {
  return collection.update(query(id), {$set: {
    status: status,
    errorMessage: errorMessage,
    executable: executable
  }})
}
