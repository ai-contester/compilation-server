FROM    openjdk:8-jdk-alpine

RUN     apk add --update --no-cache nodejs

COPY    . /app/
RUN     cd /app \
        && npm config set strict-ssl false \
        && npm install \
        && rm -rf /var/cache/apk/* \
        && mkdir /strategy-templates

EXPOSE  3200

WORKDIR /app/
CMD     ["node", "index.js"]
