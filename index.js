'use strict'

var connect = require('connect')
var http = require('http')
var bodyParser = require('body-parser')
var config = require('./config.json')

var db = require('./mongo-helper')

var app = connect()
    .use(bodyParser.json())
    .use('/compile', handleRequest)

if (process.env.NODE_ENV === 'development') {
  console.log('development mode')
  var errorHandler = require('errorhandler')
  app.use(errorHandler())
};

http.createServer(app).listen(config.port)
console.log('compilation-server running on port ' + config.port)

let temp = require('temp')
let fs = require('fs')
let cp = require('child_process')
let os = require('os')
let path = require('path')

function handleRequest (req, res) {
  if (!req.body.id) {
    res.writeHead(400, { 'Content-Type': 'application/json' })
    res.write(JSON.stringify({message: 'Need to strategy id'}))
    res.end()
    return
  }
  db.getStrategy(req.body.id).then(function (strategy) {
    if (!strategy) {
      res.writeHead(404, { 'Content-Type': 'application/json' })
      res.write(JSON.stringify({message: 'Strategy ' + req.body.id + ' not found'}))
      res.end()
      return
    }
    res.writeHead(202, { 'Content-Type': 'application/json' })
    res.write(JSON.stringify({message: 'Sended to compilation'}))
    res.end()
    compile(strategy)
  })
}

function compile (strategy) {
  console.log('COMPILE!')
  var tDir = temp.mkdirSync({})
  console.log(tDir)
  var sourcePath = tDir + '/MyStrategy.java'
  var classPath = tDir + '/MyStrategy.class'

  fs.writeFileSync(sourcePath, strategy.source)
  console.log('files:')
  console.log(fs.readdirSync(tDir))
  // HACK!!!
  var binPath = '"'
  var templatePath = config.java.template
  if (!path.isAbsolute(templatePath)) {
    templatePath = process.cwd() + '/' + templatePath
  }
  var command = binPath +
    'javac" -encoding utf8 -implicit:none ' +
    '-sourcepath ' + templatePath +
    ' MyStrategy.java'
  if (os.type() === 'Windows_NT') {
    command = 'chcp 65001 | ' + command
  }
  console.log(command)

  cp.exec(command, { cwd: tDir }, (err, stdout, stderr) => {
    console.log(`stdout: ${stdout}`)

    var mess = stderr.toString()
    console.log(`stderr: ` + mess)
    var response = {}
    if (err) {
      console.log(err.message)
      response.status = 'error'
      response.errorMessage = mess
    } else {
      response.status = 'accepted'
      response.executable = fs.readFileSync(classPath)
    }

    db.updateStrategy(strategy._id, response.status, response.errorMessage, response.executable)

    ifExistRemoveSync(sourcePath)
    ifExistRemoveSync(classPath)
    fs.rmdirSync(tDir)
  })
}

function ifExistRemoveSync (path) {
  if (fs.existsSync(path)) {
    fs.unlinkSync(path)
  }
}
